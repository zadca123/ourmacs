(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Add local packages
(add-to-list 'load-path "~/.emacs.d/local")

;; ;;idk wtf something with terminal emacs
;; (set-keyboard-coding-system nil)

;; Loads config
(when (file-readable-p "~/.emacs.d/config.org")
  (org-babel-load-file (expand-file-name "~/.emacs.d/config.org")))

;; Move customization variables to a separate file and load it
(setq custom-file (locate-user-emacs-file "~/.emacs.d/custom-vars.el"))
(load custom-file 'noerror 'nomessage)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
